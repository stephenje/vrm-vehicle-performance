#!/usr/bin/env groovy

env.APPLICATION_NAME = 'vrm-vehicle-performance'
env.GIT_BRANCH = 'master'
env.BITBUCKET_CREDENTIALS_ID = 'bitbucket-ro'
env.MAVEN_RO_CREDENTIALS_ID = 'maven-ro'
env.SLACK_CHANNEL = '#pipeline-performance'
env.GIT_REPOSITORY_PATH = "bitbucket.org/motabilityoperations/${env.APPLICATION_NAME}.git"
env.WORKSTREAM_NAME = "VRM:Vehicle:${env.TARGET_BUILD_ENVIRONMENT}"
env.SLACK_HEADING = "VRM Vehicle Pipeline Performance"
def performanceResponse
def testName
def reportName
def serviceName

def notifySlack(String buildStatus = 'STARTED', String performanceResponse) {
  buildStatus = buildStatus ?: 'SUCCESS' // Build status of null means success.

  String color
  switch (buildStatus) {
    case 'STARTED':
      color = '#D4DADF'
      break
    case 'APPROVAL':
      color = '#0072BC'
      break
    case 'SUCCESS':
      color = '#BDFFC3'
      break
    case 'UNSTABLE':
      color = '#FFFE89'
      break
    default:
      color = '#FF9FA1'
      break
  }

  def resultBefore = currentBuild.result
  def lastBuildStatus = currentBuild.getPreviousBuild()?.getResult()

  try {
    slackSend(channel: env.SLACK_CHANNEL, color: color, message: "${env.SLACK_HEADING} - ${env.TARGET_BUILD_ENVIRONMENT} - ${env.TEST_TYPE}\n" + buildStatus + performanceResponse)
  } catch (exception) {
    currentBuild.result = resultBefore
  }
}

def label = 'mo-performance'
podTemplate(
        label: label,
        cloud: 'openshift',
        containers: [
                containerTemplate(
                        name: 'jnlp',
                        image: "docker-registry.default.svc:5000/devops/jenkins-slave-mo-performance:1.7.2",
                        workingDir: '/tmp',
                        args: '${computer.jnlpmac} ${computer.name}',
                        serviceAccount : 'jenkins',
                        command: '',
                ),
        ],
        volumes: [emptyDirVolume(mountPath: '/dev/shm', memory: true)]
) {

  try {

    node(label) {

      stage('Initialise') {
        /* Checkout the scripts */
        checkout scm: [
                $class: 'GitSCM',
                userRemoteConfigs: [
                        [
                                url: "https://stephenje@bitbucket.org/stephenje/${env.APPLICATION_NAME}.git",
                                credentialsId: env.BITBUCKET_CREDENTIALS_ID
                        ]
                ],
                branches: [[name: env.GIT_BRANCH]]
        ], poll: false
      }

      stage('Build Configuration File') {
        /* Build the Taurus configuration file to manage output to Blazemeter */
        String tarusFileString = 'modules:\n' +
                '  blazemeter:\n' +
                '    token: 423939697f483f1c1fbebd85:872b9917122a300a91bfe80f1289d5b83990f83d2600ba306197fca9bb73c9ddb65073d0'

          /* Write the file */
          writeFile file: '/home/jenkins/.bzt-rc', text: tarusFileString
      }

      stage('Execute Performance Tests') {
        dir("${WORKSPACE}/src/tests/${env.TRIGGER_BUILD_JOB_NAME}") {
          def files
          files = findFiles()
          files.each { fileinfo ->
            if (fileinfo.name.endsWith(".yaml") && !fileinfo.name.contains("jmx")) {
              if (env.TEST_TYPE == "service") {
                testName = fileinfo.name[0..-6]
                reportName = new Date().format('dd-MM-yyyy HH:mm:ss').toString()
                /* Execute the Taurus test, we surpress the output with the -q option */
                /* We overwrite the target environment from the jenkins parameter */
                sh "bzt " + fileinfo.name + " -o modules.jmeter.properties.service-environment=${env.TARGET_BUILD_ENVIRONMENT} -o modules.blazemeter.project=\"" + env.WORKSTREAM_NAME + "\" -o modules.blazemeter.test=" + testName + " -o modules.blazemeter.report-name=\"" + reportName + "\" || echo Taurus Completed"
              }
            }
          }
        }
      }

      stage('Analyse Results') {

        /* Execute the slack string builder JAR. This queries the BlazeMeter API's to build the test status */
        dir("${WORKSPACE}/src/reporting") {

          /* Execute the Jar file */
          sh "java -jar blazemeter-error-reporting.jar ${env.WORKSTREAM_NAME} ${env.TRIGGER_BUILD_JOB_NAME} ${env.TRIGGER_BUILD_JOB_NUMBER} ${env.JOB_NAME} ${env.BUILD_NUMBER} ${env.JENKINS_URL} ${env.TEST_TYPE} not-checked not-checked all> blazemeter_performance_digest.out"
          performanceResponse = readFile 'blazemeter_performance_digest.out'
        }
      }

    }

  } catch (exception) {
    currentBuild.result = 'FAILURE'
    throw exception
  } finally {
    notifySlack(currentBuild.result, performanceResponse)
  }
}